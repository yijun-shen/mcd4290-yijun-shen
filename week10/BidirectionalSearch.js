/* Write your function for implementing the Bi-directional Search algorithm here */
function BidirectionalSearchTest(data){
    let mid = data.length / 2
    for(let i = 0; i < mid; i++){
        if(data[mid+i].emergency === true){
            return data[mid].address    
        }
        if(data[mid-i].emergency === true){
            return data[mid].address 
        }
    }
    return null

}